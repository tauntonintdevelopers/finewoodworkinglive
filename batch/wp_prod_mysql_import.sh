#!/bin/bash
# Import Prod MYSQL Data

echo "Start Import!"
now=$(date +"%m_%d_%Y")
mysqldump --user='fwwlive' --password='8Mfc7KYCyyvbcsDT' -h wpressmysql.tauntoncloud.com taunton_finewoodworkinglive_prod > /srv/sites/finewoodworkinglive/backups/FWWLIVE_WP_PROD_$now.sql
gzip /srv/sites/finewoodworkinglive/backups/FWWLIVE_WP_PROD_$now.sql
echo "End Import!"

# Delete 1 week old backups
find /srv/sites/finewoodworkinglive/backups/FWWLIVE_WP_PROD* -mtime +7 -exec rm {} \;

<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

$root_dir = dirname(__DIR__);
$webroot_dir = $root_dir . '/web';

/**
 * Set up our global environment constant and load its config first
 * Default: development
 */
$host = php_uname('n');
switch ($host) {
	case 'wpappsrv01':
		define('WP_ENV', 'production');
		define('WP_SERVER', 'web1');
		break;
	case 'wpappsrv02':
		define('WP_ENV', 'production');
		define('WP_SERVER', 'web2');
		break;
	case 'appserver-1c':
		define('WP_ENV', 'development');
		break;
	case 'stage.appsrv1':
		define('WP_ENV', 'stage');
		break;
	case 'homestead':
		define('WP_ENV', 'vagrant');
		define('IS_HOMESTEAD', true);
		break;	
	case 'vagrant-ubuntu-trusty-64':
		define('WP_ENV', 'vagrant');
		break;
	case 'ip-10-0-2-61':
	    define('WP_ENV', 'development-php8');
	    break;	
	default:
		define('WP_ENV', 'production');
		define('WP_SERVER', 'web1');
		break;
}

if( WP_ENV == 'production' ){
	$env_config = __DIR__ . '/environments/'. WP_ENV."-".WP_SERVER.'.php';
}else{
	$env_config = __DIR__ . '/environments/'. WP_ENV.'.php';
}
if (file_exists($env_config)) {
	require_once $env_config;
}else{
	die( WP_ENV. "configuration file not found");
}


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

define('AUTOMATIC_UPDATER_DISABLED', true);

define('WP_POST_REVISIONS', 1);

define('WP_MEMORY_LIMIT', '256M');
define('WP_MAX_MEMORY_LIMIT', '256M');

/*
 * 
 * Custom Content Directory
 */
define('CONTENT_DIR', '/app');
define('WP_CONTENT_DIR', $webroot_dir . CONTENT_DIR);
define('WP_CONTENT_URL', WP_HOME . CONTENT_DIR);



/**
 * Custom Settings
 */
define('AUTOMATIC_UPDATER_DISABLED', true);
define('DISALLOW_FILE_EDIT', true);

/**
 * Bootstrap WordPress
 */
if (!defined('ABSPATH')) {
	define('ABSPATH', $webroot_dir . '/wp/');
}
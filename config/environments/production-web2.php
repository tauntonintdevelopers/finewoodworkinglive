<?php
/* Devel System Configuration */

/* SITE URL */
define('WP_HOME', 'http://www.finewoodworkinglive.com');
define('WP_SITEURL', 'http://www.finewoodworkinglive.com/wp');

/*
 * DATABASE Configuration
 *
 */
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'taunton_finewoodworkinglive_prod');

/** MySQL database username */
define('DB_USER', 'fwwlive');

/** MySQL database password */
define('DB_PASSWORD', '8Mfc7KYCyyvbcsDT');

/** MySQL hostname */
define('DB_HOST', 'wpressmysql.tauntoncloud.com');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Added by W3 Total Cache */
define('WP_CACHE', true);

if (function_exists('xdebug_disable')) {
    xdebug_disable();
}

define('WP_DEBUG', false);
define('WP_DEBUG_LOG', false);
define('WP_DEBUG_DISPLAY', false);
define('DISABLE_WP_CRON', true);

//define( 'CUSTOM_TAGS', true );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

define('AUTH_KEY', '?L+%SjR|2>+!Ag$c79O~E2B7:Aeo^|Y-5kRvc;||7cNDAgt1D*(i+9/L3?^Rad%m');
define('SECURE_AUTH_KEY', 'y&{6DTcC |pf!x-,u&@9*)b-pT4~K ,@,d/([3vT(.$?wW,$?[VK/J;5m-eMmVt/');
define('LOGGED_IN_KEY', ',qjg[X|9^QLb+w3JQqhl@@}LuxXV!1|=e|;M Y:w!$hK:-l14,+(Lxd2Q2]>QVc}');
define('NONCE_KEY', '6Yh]] @A,oM68geeJa u&SLqY-rZ9+nwye% Ln&-k5}+<T+8=d~ XvdWP@~c]X/3');
define('AUTH_SALT', 'PN,Vi#00j$&:G(lea48*|U:v0LeE5O,^&ev(MyB>_7Zs[7oJjEyxEF_O,8 (e#A*');
define('SECURE_AUTH_SALT', '7H.OQZ+? @&&R}@;H#C6sa_i+Q^,4.<ReZT-Mi4LpD FUCG~3~Cnzid<3RyEn;ZO');
define('LOGGED_IN_SALT', 'Y(k]n&/--1tkUh:f&$U:J6U<]gNvAr>k{-2@-PuG$iXka(Y!I.u9JUFe,<zQk]AR');
define('NONCE_SALT', 'V-*%>T,4f(Sd$%m3U3[GV~lE@+i+;+EwWoTsF!]wWIl0T!1xw}j8;,}Z|tS|;?!g');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
//$table_prefix  = 'wp_';
$table_prefix  = 'taunton_fwwlive_';

/**
 * [WP-86] Updated Privacy Policy Popup
 */
define('FWWL_UPDATED_PRIVACY_POLICY_POPUP', true);

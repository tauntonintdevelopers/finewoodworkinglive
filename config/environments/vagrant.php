<?php
/* Devel System Configuration */

/* SITE URL */
define('WP_HOME', 'http://www.finewoodworkinglive.local');
define('WP_SITEURL', 'http://www.finewoodworkinglive.local/wp');

/*
 * DATABASE Configuration
 *
 */
// ** MySQL settings - You can get this info from your web host ** //

if (defined('IS_HOMESTEAD') && IS_HOMESTEAD == true) {
    /** The name of the database for WordPress */
    define('DB_NAME', 'fwwlive');

    /** MySQL database username */
    define('DB_USER', 'homestead');

    /** MySQL database password */
    define('DB_PASSWORD', 'secret');
} else {
    /** The name of the database for WordPress */
    define('DB_NAME', 'fwwl');
    
    /** MySQL database username */
    define('DB_USER', 'root');

    /** MySQL database password */
    define('DB_PASSWORD', 'root');
}

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Added by W3 Total Cache */
define('WP_CACHE', false);

if (function_exists('xdebug_disable')) {
    xdebug_disable();
}

define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', false);
define('DISABLE_WP_CRON', true);

//define( 'CUSTOM_TAGS', true );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

define('AUTH_KEY', '3`:X1GhnBLGQTv;1>G%}z88 OmO+;7K~=@%$EzxgYi|}e{q&-FTl[Zx6/<BBNLV!');
define('SECURE_AUTH_KEY', '~_IIA|+ZZSNQzx RCS]S|r;~r.Gq|#Nx{:KRkP:0-)p5FDQ!/+:_H<o_VO!YAq+U');
define('LOGGED_IN_KEY', 'PtfNox+7(v+1fc8rKzMPw*bjEqFKKOnFK)p%l`_!NLhltlfAzK*1F_~P@Jbrj#._');
define('NONCE_KEY', 'n#ZZ-kVW++dyZLhVqKsHR-6;8/LNf+Z&s#{}]]&RUd1&Q=Ro2`.Te3kQV?els||!');
define('AUTH_SALT', '7p|,Khq(47x|ogSuEMKTbma{|q&}7vL]+WI=,naXBUiy &RT&HK2HMF)0Zh~~nK*');
define('SECURE_AUTH_SALT', '1_zCTw;`<CzU)FO1NRKo.^L!.3@zAcU&>=zP$xXZ1dYdZ$QIpgtXs7o_B*b]*h> ');
define('LOGGED_IN_SALT', '=o=4jiPf+-R=xD{D+WmsgTNAesW|+$U<HI~Y`Z}hBeXHMX`fG:P?6-[+Ya.|5jO+');
define('NONCE_SALT', '`e8JY5DB(||Y.Oooa1Ri!Ph+I*M_i^IXF/~zf;n8Ldo1L+jRJki]sfVn{Z|E6;qc');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
//$table_prefix  = 'wp_';
$table_prefix  = 'taunton_fwwlive_';

/**
 * [WP-86] Updated Privacy Policy Popup
 */
define('FWWL_UPDATED_PRIVACY_POLICY_POPUP', true);

var ttn_sweepstakes;
(function($){
  $(document).ready(function(){

    ttn_sweepstakes = {

      handle_submit: function(form){
        $('.submit-button').parent().addClass('finished');
        var formData = {
          action: 'ttn_sweepstakes',
        };

        var $form = $(form),
            email = $form.find('input[id=email]').val(),
            fname = $form.find('input[id=firstname]').val(),
			minitial = $form.find('input[id=initial]').val(),
            lname = $form.find('input[id=lastname]').val(),
			add1 = $form.find('input[id=address1]').val(),
			add2 = $form.find('input[id=address2]').val(),
			city = $form.find('input[id=city]').val(),
			country = $form.find('select[id=country]').val(),
			state = $form.find('select[id=province]').val(),
			zip = $form.find('input[id=zip]').val(),
			phone = $form.find('input[id=phone]').val(),
			essay = $form.find('textarea[id=essay]').val(),
            taunton_optin = $form.find('input[id=taunton-optin]').prop('checked'),
			partner =  $form.find('input[id=partner]').val(),
            source = $form.data('source'),
            sweepstakes = $form.data('name'),
            ajax_url =  $form.data('ajax-url') +'/wp-admin/admin-ajax.php';
			
		var pref = $form.data('pref');
		var ajax_nonce = $form.data('ajax_nonce');
		var partner_optin = null;
		if (partner) {
			partner_optin = $form.find('input[id=' + partner + ']').prop('checked');
		}

        function showThankyou() {
          $form.find('.formfield--submit').addClass('finished');
        }

        if(sweepstakes) {
		  formData['selections'] = selections_list(sweepstakes, $form);
          formData['sweepstakes'] = sweepstakes;
        }        
        if (email) {
            formData['email'] = email;
        }
        if (fname) {
          formData['firstname'] = fname;
        }
		if (minitial) {
		  formData['middleinitial'] = minitial;	
		}
        if (lname) {
          formData['lastname'] = lname;
        }
		if (add1) {
          formData['address1'] = add1;
		}
		if (add2) {
          formData['address2'] = add2;
		}
		if (city) {
          formData['city'] = city;
		}
		if (country) {
          formData['country'] = country;
		}
		if (state) {
          formData['state'] = state;
		}
		if (zip) {
          formData['zip'] = zip;
		}
		if (phone) {
          formData['phone'] = phone;
		}
		if (essay) {
          formData['essay'] = essay;
		}
        if(taunton_optin) {
          formData['taunton_optin'] = taunton_optin;
        }
		if(partner) {
          formData['partner'] = partner;
        }
        if(partner_optin) {
          formData['partner_optin'] = partner_optin;
        }
		if (source) {
          formData['source'] = source;
        }
		if (ajax_nonce) {
			formData['ajax_nonce'] = ajax_nonce;
		}
    			
        $.ajax({
          type: 'POST',
          url: ajax_url,
          data: formData,
          complete: function(){
            console.log(formData);
			
			if( pref ) {
			
				formData['pref'] = pref;
				formData['action'] = 'taunton_email_subscribe'
				
				$.ajax({
					  type: 'POST',
					  url: '/wp/wp-admin/admin-ajax.php',
					  data: formData
				 });
			
			}
		
            showThankyou();
          }
        });
	  }
	  
	}
	
	
	//handle non conform field here and push into selections field
	//based on sweeptakes value
	function selections_list(sweepstakes, form){
		var selections = {};
		
		$form = $(form);
		console.log(sweepstakes);
		switch (sweepstakes) {
			case 'fwwl_2017_scholarship':
      case 'fwwl_2018_scholarship':
				var tmp = []
				if ($form.find('input[id=military]').prop('checked')) {
					selections.military = 1;
					selections.yearsinservice = $form.find('input[id=yearsinservice]').val();
					selections.stationed = $form.find('input[id=stationed]').val();
					selections.branch = $form.find('input[id=branch]').val();
					
				} else {
					selections.military = 0;
				}
				break;	
		}
		console.log(selections);
		return selections;
	}
       
  })
})(jQuery);
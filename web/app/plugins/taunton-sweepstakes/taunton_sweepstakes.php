<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
/*
Plugin Name: Taunton Sweepstakes
Description: Taunton Sweepstakes Wordpress Plugin.
*/ 

/*
 * Usage
 * =====
 * Sweepstakes functionality, backend processing and admin
 *
 */

 /*
* Sweepstakes Optin
*/
add_action( 'wp_ajax_ttn_sweepstakes', '_ajax_ttn_sweepstakes' );
add_action('wp_ajax_nopriv_ttn_sweepstakes', '_ajax_ttn_sweepstakes');
function _ajax_ttn_sweepstakes() {
	$data = array();
	$data['firstname'] = trim( $_POST['firstname']);
	$data['middleinitial'] = trim( $_POST['middleinitial']);
	$data['lastname'] = trim( $_POST['lastname']);
	$data['email'] = trim( $_POST['email']);
	$data['address1'] = trim( $_POST['address1']);
	$data['address2'] = trim( $_POST['address2']);
	$data['city'] = trim( $_POST['city']);
	$data['country'] = trim( $_POST['country']);
	$data['state'] = trim( $_POST['state']);
	$data['zip'] = trim( $_POST['zip']);
	$data['lastname'] = trim( $_POST['lastname']);
	$data['phone'] = trim( $_POST['phone']);
	$data['partner'] = trim( $_POST['partner']);;
	$data['sweepstakes'] = trim( $_POST['sweepstakes']);;
	$data['source'] = trim( $_POST['source']);
		

	if( !empty( $_POST['selections'] ) ){	
		$data['selections'] = serialize( $_POST['selections'] );	
	}else{
		$data['selections'] = '';
	}
	if( !empty( $_POST['essay'] ) ){	
		$data['essay'] = $_POST['essay'];	
	}else{
		$data['essay'] = '';
	}
	
	if( !empty( $_POST['taunton_optin'] ) ){	
		$data['taunton_optin'] = 1;
	}

	if( !empty( $_POST['partner_optin'] ) ){	
		$data['partner_optin'] = 1;
	}
	
	TTN_Sweepstakes::insert( $data );
	unset( $data );
	print array( 'result' => 'success' );
	die;	
}


// regsiter dependant JS files
function register_sweepstakes_dependancies()
{
   // Register the script
	wp_register_script( 'taunton-sweepstakes', plugins_url( '/js/sweepstakes.js', __FILE__ ) );
    wp_enqueue_script( 'taunton-sweepstakes' );
}
add_action( 'wp_enqueue_scripts', 'register_sweepstakes_dependancies');

define( 'TAUNTON_SWEEPSTAKES_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

require_once(dirname (__FILE__) . '/lib/Sweepstakes.php');
if( is_admin() ){
	require_once(dirname (__FILE__) . '/lib/admin/Sweepstakes.php');
}

function taunton_sweepstakes_form_open($atts) {
	// normalize attribute keys, lowercase
	$atts = array_change_key_case((array)$atts, CASE_LOWER);
	
	// override default attributes with user attributes
    $taunton_email_atts = shortcode_atts( array(
        'id' => 'taunton-sweepstakes__form',
        'class' => 'contest__form',
		'name' => 'default_sweepstakes',
		'source' => 'default_sweepstakes',
		'pref' => site_email_signup_list(),
    ), $atts );
	
	$form_str = '<form id="' . $taunton_email_atts['id'] . '" class="' . $taunton_email_atts['class'] . '" data-ajax-url="/wp" data-name="' . $taunton_email_atts['name'] . '" data-source="' . $taunton_email_atts['source'] . '" data-pref="' . $taunton_email_atts['pref'] . '" data-ajax_nonce="' . taunton_email_wp_nonce() . '">';

    return $form_str;

}

function taunton_sweepstakes_form_open_init()
{
    add_shortcode('taunton_sweepstakes_form_open', 'taunton_sweepstakes_form_open');
}
 
add_action('init', 'taunton_sweepstakes_form_open_init');

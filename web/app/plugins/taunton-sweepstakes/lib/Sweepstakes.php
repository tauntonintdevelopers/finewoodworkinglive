<?php 

class TTN_Sweepstakes{

	function __construct(){
	}
	
	static function insert( $data ){
		
			
		global $wpdb;
		
		$db_table = $wpdb->prefix.'sweepstakes_optins';
		
		if( !self::check_table_exists() ){
			self::create_table();
		}
	
		error_log( print_R( $data, true ) );
		
		$result = $wpdb->insert(
					$db_table,
					array(	
							'sweepstakes' =>  $data['sweepstakes'],						
							'partner' =>  $data['partner'],						
                            'firstname' =>  $data['firstname'],
							'middleinitial' =>  $data['middleinitial'],
                            'lastname' =>  $data['lastname'],
                            'email' =>  $data['email'],
							'address1' =>  $data['address1'],
							'address2' =>  $data['address2'],
							'city' =>  $data['city'],
							'country' =>  $data['country'],
							'state' =>  $data['state'],
							'zip' =>  $data['zip'],
							'phone' =>  $data['phone'],
							'selections' =>  $data['selections'],	
							'essay' =>  $data['essay'],	
							'taunton_optin' =>  $data['taunton_optin'],		
 							'partner_optin' =>  $data['partner_optin'],		
                           	'create_date' => date('Y-m-d h:i:s'),
					),
					array(
							'%s',//sweepstakes
							'%s',//partner
							'%s',//fn
							'%s',//mi					
							'%s',//ln
							'%s',//em
							'%s',//ad1
							'%s',//ad2
							'%s',//ci
							'%s',//cou
							'%s',//st
							'%s',//zi
							'%s',//ph
							'%s',//s
							'%s',//ess
							'%d',//t op
							'%d',//p op
							'%s',//da
					)
					);
					
		error_log( "WP Insert $db_table --> $result ". $wpdb->last_error );
	}
		
	function check_table_exists(){

		global $wpdb;

		$db_table = $wpdb->prefix.'sweepstakes_optins';

		if( $wpdb->get_var("SHOW TABLES LIKE '$db_table'") == $db_table) {
			return true;
		}

		return false;	
	}
	
	function create_table(){
		
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		
		global $wpdb;

		$db_table = $wpdb->prefix.'sweepstakes_optins';
		
		$charset_collate = $wpdb->get_charset_collate();

		$db_table_sql = "CREATE TABLE {$db_table} (
id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
sweepstakes varchar(60) NOT NULL,
partner varchar(60) NOT NULL,
firstname varchar(50) NOT NULL,
middleinitial varchar(2) NULL DEFAULT '',
lastname varchar(50) NOT NULL,
email varchar(255) NOT NULL DEFAULT '',
address1 varchar(255) NOT NULL DEFAULT '',
address2 varchar(255) NULL DEFAULT '',
city varchar(35) NOT NULL DEFAULT '',
country varchar(2) NOT NULL DEFAULT '',
state varchar(2) NOT NULL DEFAULT '',
zip varchar(12) NOT NULL DEFAULT '',
phone varchar(30) NULL DEFAULT '',
taunton_optin tinyint(1) DEFAULT NULL,
partner_optin tinyint(1) DEFAULT NULL,
selections text DEFAULT NULL,
essay text DEFAULT NULL,
create_date datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
PRIMARY KEY (id),
KEY email (email)						
) {$charset_collate};";
		
		$result = dbDelta( $db_table_sql );
		
	}
}
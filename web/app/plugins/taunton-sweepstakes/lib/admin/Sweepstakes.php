<?php
/**
 FHB Sweepstakes-Optin
 */
if (!class_exists('WP_List_Table')) {
	require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

require_once(TAUNTON_SWEEPSTAKES_PLUGIN_PATH . '/lib/phpexcel/PHPExcel.php');

/**
 * FHB Sweepstakes Opt-In List
 */
class TTN_Sweepstakes_List_Table extends WP_List_Table
{
	
	function __construct()
	{
		global $status, $page;

		parent::__construct(array(
				'singular' => 'Taunton Sweepstakes',
				'plural' => 'Taunton Sweepstakes',
		));
	}

	/**
	 get table colum default
	 */
	function column_default($item, $column_name)
	{
		return $item[$column_name];
	}

	/**
	 Table Columns
	 */
	function get_columns()
	{
		$columns = array(
				'partner' => 'Partner',
				'firstname' => 'First Name',
				'middleinitial' => 'Middle Initial',
				'lastname' => 'Last Name',
				'address1' => 'Address 1',
				'address2' => 'Address 2',
				'city' => 'City',
				'state' => 'State',
				'country' => 'Country',
				'zip' => 'Zip',
				'phone' => 'Phone',
				'email' => 'Email',				
				'taunton_optin' => 'Taunton Optin',
				'partner_optin' => 'Partner Optin',
				'selections' => 'Selections',
				'essay' => 'Essay/Free Form Text',
				'create_date' => 'Created'
		);
		return $columns;
	}
		
	function column_sweepstakes( $item ){
		$link = '/wp/wp-admin/admin.php?page=sweepstakes_optin&sweepstakes='.$item->sweepstakes;
		$sweepstakes = ucfirst($item->sweepstakes);
		$sweepstakes = preg_replace( "/_/", " ", $sweepstakes );
		$sweepstakes = sprintf( "<a href='%s'>%s</a>", $link, $sweepstakes );
		return ucwords( $sweepstakes );
	}
	
	function column_taunton_optin( $item ){
		return $item['taunton_optin'] == 1 ? 'Yes' : 'No';
	}
	
	function column_partner_optin( $item ){
		return $item['partner_optin'] == 1 ? 'Yes' : 'No';
	}
	
		
	function column_create_date( $item ){
		return date( 'Y/m/d', strtotime( $item['create_date'] ) );
	}
	
	function column_selections( $item ){
		$selstr = '';
		$sel = unserialize($item['selections']);
		if ($sel) {
			foreach ($sel as $i => $v) {
				if (is_array($v)) {
					$v = implode(",",$v);	
				}
				if (is_numeric($i)) {
					$selstr .= ',' . $v;
				} else {
					$selstr .= ',' . $i . ':' . $v;	
				}
			}
			$selstr =  substr($selstr, 1);
		}
		return $selstr;	
	}

	function get_sweepstakes_columns(){
		$columns = array(
				'sweepstakes' => 'Sweepstakes',
		);
		return $columns;
	}
	
	/**
	* Sortable columns
	*/
	function get_sortable_columns()
	{
		$sortable_columns = array(
				'firstname' => array('firstname', true),
				'lastname' => array('lastname', false),
				'create_date' => array('create_date', false),
		);
		return $sortable_columns;
	}

	function get_sortable_sweepstakes_columns(){
		return array();
	}
		
	/**
	* Bulk Actions 
	*/
	function get_bulk_actions()
	{
		$actions = array();
		return $actions;
	}

	/**
	 process bulk action
	 */
	function process_bulk_action()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . 'sweepstakes_optins';

		if ('delete' === $this->current_action()) {
			$ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
			if (is_array($ids)) $ids = implode(',', $ids);

			if (!empty($ids)) {
				/* Delete User Table */
				$wpdb->query("DELETE FROM $table_name WHERE id IN($ids)");				
			}
			
		}
	}

	function get_sweepstakes(){
		
		global $wpdb;
		
		$table_name = $wpdb->prefix . 'sweepstakes_optins';
		
		$per_page = 20;
		
		$columns = $this->get_sweepstakes_columns();
		
		$hidden = array();
		
		$sortable = $this->get_sortable_sweepstakes_columns();
		
		//configure table headers, defined in our methods
		$this->_column_headers = array($columns, $hidden, $sortable);
		
		//process bulk action
		$this->process_bulk_action();
		
		$this->items = $wpdb->get_results( "SELECT distinct(sweepstakes) FROM $table_name ORDER BY create_date DESC" );
		
	}	
	
	
	
	/**
	 * Get Members
	 */
	function prepare_items()
	{
		global $wpdb;
		
		$table_name = $wpdb->prefix . 'sweepstakes_optins';

		$per_page = 20; 

		$columns = $this->get_columns();
		
		$hidden = array();
		
		$sortable = $this->get_sortable_columns();

		//configure table headers, defined in our methods
		$this->_column_headers = array($columns, $hidden, $sortable);

		//process bulk action
		$this->process_bulk_action();

		// total optin
		$sweepstakes = $_REQUEST['sweepstakes'];
		
		$total_items = $wpdb->get_var("SELECT COUNT(id) FROM $table_name WHERE sweepstakes ='$sweepstakes'");

		// prepare query params
		$paged = isset($_REQUEST['paged']) ? max(0, intval($_REQUEST['paged']) - 1) : 0;
		
		$offset = $paged * $per_page;
		
		$orderby = (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array_keys($this->get_sortable_columns()))) ? $_REQUEST['orderby'] : 'create_date';
		
		$order = (isset($_REQUEST['order']) && in_array($_REQUEST['order'], array('asc', 'desc'))) ? $_REQUEST['order'] : 'desc';

		$this->items = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE sweepstakes ='%s' ORDER BY $orderby $order LIMIT %d OFFSET %d",$sweepstakes, $per_page, $offset), ARRAY_A);
		
		// [REQUIRED] configure pagination
		$this->set_pagination_args(array(
				'total_items' => $total_items, // total items defined above
				'per_page' => $per_page, // per page constant defined at top of method
				'total_pages' => ceil($total_items / $per_page) // calculate pages count
		));
	}
	
	function download_sweepstakes_excel(){
		
		global $wpdb;
		
		$table_name = $wpdb->prefix . 'sweepstakes_optins';
		
		$items = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE sweepstakes ='%s' ORDER BY create_date desc",$_REQUEST['sweepstakes']));
		
		return $items;
	}
	
}

/**
Admin Page 
*/

/**
 * FHB Sweepstakes-Optin Admin Menu
 */
function ttn_sweepstakes_optin_admin_menu()
{
	add_menu_page(__('Sweepstakes Opt-ins', 'sweepstakes_optin'), __('Sweepstakes Opt-ins', 'sweepstakes_optin'), 'moderate_comments', 'sweepstakes_optin', 'ttn_sweepstakes_optin_page_handler');
	
}

add_action('admin_menu', 'ttn_sweepstakes_optin_admin_menu');

function ttn_sweepstakes_optin_redirect() {


	if ( isset( $_REQUEST['page']) && ($_REQUEST['page'] == 'sweepstakes_optin' ) ){

		if( !empty( $_REQUEST['sweepstakes'] ) &&  !empty( $_REQUEST['download'] ) ){

			$sweepstakes = $_REQUEST['sweepstakes'];
						
			$table = new TTN_Sweepstakes_List_Table();
			
			$rows = $table->download_sweepstakes_excel();
			
			// Create new PHPExcel object
			$objPHPExcel = new PHPExcel();
			
			// Set document properties
			$objPHPExcel->getProperties()->setCreator("Taunton FinewoodworkingLive")
			->setLastModifiedBy("Taunton FinewoodworkingLive")
			->setTitle("FinewoodworkingLive Sweepstakes")
			->setSubject("FinewoodworkingLive Sweepstakes")
			->setDescription("FinewoodworkingLive Sweepstakes")
			->setKeywords("FinewoodworkingLive Sweepstakes")
			->setCategory("FinewoodworkingLive Sweepstakes");
			
			
			// set column titles
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Sweepstakes')
			->setCellValue('B1', 'Partner')
			->setCellValue('C1', 'First Name')
			->setCellValue('D1', 'Middle Initial')
			->setCellValue('E1', 'Last Name')
			->setCellValue('F1', 'Address 1')
			->setCellValue('G1', 'Address 2')
			->setCellValue('H1', 'City')
			->setCellValue('I1', 'State')
			->setCellValue('J1', 'Country')
			->setCellValue('K1', 'Zip')
			->setCellValue('L1', 'Email')
			->setCellValue('M1', 'Phone')
			->setCellValue('N1', 'Selections')
			->setCellValue('O1', 'Free Form Text')
			->setCellValue('P1', 'Partner Optin')
			->setCellValue('Q1', 'Taunton Optin')
			->setCellValue('R1', 'Date');
			
			$objPHPExcel->setActiveSheetIndex(0);
			
			$j = 1;
			for ($i = 0; $i < count($rows); $i++) {
				
				$j = $j +1;
				
				$selstr = '';
				$sel = unserialize($rows[$i]->selections);
				if ($sel) {
					foreach ($sel as $index => $v) {
						if (is_array($v)) {
							$v = implode(",",$v);	
						}
						if (is_numeric($index)) {
							$selstr .= ',' . $v;
						} else {
							$selstr .= ',' . $index . ':' . $v;	
						}
					}
					$selstr =  substr($selstr, 1);
				}

				$objPHPExcel->getActiveSheet()->setCellValue('A' . $j, property_exists($rows[$i],'sweepstakes')?$rows[$i]->sweepstakes: '');
				$objPHPExcel->getActiveSheet()->setCellValue('B' . $j, property_exists($rows[$i],'partner')?$rows[$i]->partner: '');
				$objPHPExcel->getActiveSheet()->setCellValue('C' . $j, property_exists($rows[$i],'firstname')?$rows[$i]->firstname: '');
				$objPHPExcel->getActiveSheet()->setCellValue('D' . $j, property_exists($rows[$i],'middleinitial')?$rows[$i]->middleinitial: '');
				$objPHPExcel->getActiveSheet()->setCellValue('E' . $j, property_exists($rows[$i],'lastname')?$rows[$i]->lastname: '');
				$objPHPExcel->getActiveSheet()->setCellValue('F' . $j, property_exists($rows[$i],'address1')?$rows[$i]->address1: '');
				$objPHPExcel->getActiveSheet()->setCellValue('G' . $j, property_exists($rows[$i],'address2')?$rows[$i]->address2: '');
				$objPHPExcel->getActiveSheet()->setCellValue('H' . $j, property_exists($rows[$i],'city')?$rows[$i]->city: '');
				$objPHPExcel->getActiveSheet()->setCellValue('I' . $j, property_exists($rows[$i],'state')?$rows[$i]->state: '');
				$objPHPExcel->getActiveSheet()->setCellValue('J' . $j, property_exists($rows[$i],'country')?$rows[$i]->country: '');
				$objPHPExcel->getActiveSheet()->setCellValue('K' . $j, property_exists($rows[$i],'zip')?$rows[$i]->zip: '');
				$objPHPExcel->getActiveSheet()->setCellValue('L' . $j, property_exists($rows[$i],'email')?$rows[$i]->email: '');
				$objPHPExcel->getActiveSheet()->setCellValue('M' . $j, property_exists($rows[$i],'phone')?$rows[$i]->phone: '');
				$objPHPExcel->getActiveSheet()->setCellValue('N' . $j, $selstr);
				$objPHPExcel->getActiveSheet()->setCellValue('O' . $j, property_exists($rows[$i],'essay')?$rows[$i]->essay: '');
				$objPHPExcel->getActiveSheet()->setCellValue('P' . $j, $rows[$i]->partner_optin ? 'Yes' : 'No');
				$objPHPExcel->getActiveSheet()->setCellValue('Q' . $j, $rows[$i]->taunton_optin ? 'Yes': 'No');
				$date = date( 'Y/m/d', strtotime( $rows[$i]->create_date ) );
				$objPHPExcel->getActiveSheet()->setCellValue('R' . $j, $date );
			}
			header_remove();
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header("Content-Disposition: attachment;filename=$sweepstakes-".date('Y-m-d').".xlsx");
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('php://output');
			die;
		}
	}
}

add_action( 'admin_init', 'ttn_sweepstakes_optin_redirect' );

/**
 * FHB Sweepstakes-Optin List Page
 */
function ttn_sweepstakes_optin_page_handler()
{
	
	global $wpdb;

	$table = new TTN_Sweepstakes_List_Table();
	
	$message = '';
			
	if( !empty( $_REQUEST['sweepstakes'] ) ){
		
		$sweepstakes = $_REQUEST['sweepstakes'];
			

		$sweepstakes_label = ucfirst($sweepstakes);
		$sweepstakes_label = preg_replace( "/_/", ' ', $sweepstakes_label );
		$sweepstakes_label = ucwords( $sweepstakes_label );
		$table->prepare_items();
		
		require_once( TAUNTON_SWEEPSTAKES_PLUGIN_PATH."/templates/admin/sweepstakes_optin_list.php" );
		
	}else{
		
		$table->get_sweepstakes();
		
		require_once( TAUNTON_SWEEPSTAKES_PLUGIN_PATH."/templates/admin/sweepstakes_list.php" );
	}		
		
}
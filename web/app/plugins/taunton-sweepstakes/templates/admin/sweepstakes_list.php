<?php 
if ('delete' === $table->current_action()) {
		$message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Items deleted: %d', ''), count($_REQUEST['id'])) . '</p></div>';
	}
	?>
<div class="wrap">
    <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
    <h2>Sweepstakes Optin</h2>
    <?php echo $message; ?>

    <form id="members-table" method="GET">
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>"/>
        <?php $table->display() ?>
    </form>

</div>

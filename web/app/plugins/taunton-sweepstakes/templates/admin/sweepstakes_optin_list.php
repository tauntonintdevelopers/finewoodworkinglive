<?php 
if ('delete' === $table->current_action()) {
		$message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Items deleted: %d', ''), count($_REQUEST['id'])) . '</p></div>';
	}
	?>
<div class="wrap">

    <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
    <h2>Sweepstakes: <?=$sweepstakes_label?></h2>
    <a class="button" href="/wp/wp-admin/admin.php?page=sweepstakes_optin&sweepstakes=<?=$sweepstakes;?>&download=excel">Download Excel</a>
    <br/><br/>
    <a class="button" href="/wp/wp-admin/admin.php?page=sweepstakes_optin">Back to List</a>
    <?php echo $message; ?>

    <form id="members-table" method="GET">
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>"/>
        <?php $table->display() ?>
    </form>

</div>

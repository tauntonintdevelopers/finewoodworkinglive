$ = jQuery;
function processEmailSubscribeForm(form, widgetID) {

	var formData = {
		action: 'taunton_email_subscribe',
	};
	
	var $form = $(form),
	email = $form.find('input[type=email]').val(),
	fname = $form.find('input[id=firstname]').val(),
	lname = $form.find('input[id=lastname]').val(),
	source = $form.data('source'),
	pref = $form.data('pref'),
	ajax_nonce = $form.data('ajax_nonce'),
	ajax_url =  $form.data('ajax-url') +'/wp-admin/admin-ajax.php';
	
	var kca_sup = [];
	var pro_sup = [];
	$.each($form.find('input[name=KCA_SUP]:checked'), function () { kca_sup.push($(this).val()); });
	$.each($form.find('input[name=PRO_SUP]:checked'), function () { pro_sup.push($(this).val()); });


	if (fname) {
	  formData['firstname'] = fname;
	}
	
	if (lname) {
	  formData['lastname'] = lname;
	}
	
	if (source) {
		formData['source'] = source;
	}
	
	if (ajax_nonce) {
		formData['ajax_nonce'] = ajax_nonce;
	}
	
	if (kca_sup) {
		formData['kca_sup'] = kca_sup;
	}
	
	if (pro_sup) {
		formData['pro_sup'] = pro_sup;
	}
		
	//must have pref and email to continue
	if (pref) {
		formData['pref'] = pref;
	} else {
		return;  
	}
	
	if (email) {
		formData['email'] = email;
	} else {
		return;
	}
	
	console.log(formData); 
	
	tyDiv = $form.find('#' + widgetID + '_thankyoudiv');
	erDiv = $form.find('#' + widgetID + '_errordiv');
	ffs = $form.find('#' + widgetID + '_formfieldset');
	
    var jqxhr = $.post(ajax_url, formData, function (response) {
		if (response.success) {
            tyDiv.show();
			ffs.hide();
        } else {
			console.log(response);
            erDiv.show();
			ffs.hide();
        }
    }, 'json')
        .done(function () {
            //nothing to do here
        })
        .fail(function (response) {
			console.log(response);
            erDiv.show();
			ffs.hide();
        })
        .always(function () {
            //nothign to do here
    });

    // Perform other work here ...

    // Set another completion function for the request above
    jqxhr.always(function () {
        //nothing to do here
    });	
}

function validateSubscribeForm (form, rules, messages, widgetid){
	var $form = $(form);
	$form.validate({
		onkeyup: false,
		onfocus: false,
		onsubmit: true,
		rules: rules,
		messages: messages,
		submitHandler: function(form) {
			processEmailSubscribeForm(form, widgetid);
		}
	});
}

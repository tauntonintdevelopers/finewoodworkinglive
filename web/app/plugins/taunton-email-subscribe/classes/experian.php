<?php 
class Experian {

	function get_token() {
		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, EXPERIAN_URL_TOKEN);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		
		# Post data
		$params = array('grant_type' => 'password', 'client_id' => EXPERIAN_CLIENT_ID, 'username' => EXPERIAN_CONSUMER_KEY, 'password' => EXPERIAN_CONSUMER_SECRET);
		//This is needed to properly form post the credentials object
		foreach($params as $k => $v)
		{
		   $data .= $k . '='.urlencode($v).'&';
		}
		
		$data = rtrim($data, '&');
		
		curl_setopt($curl, CURLOPT_POST, TRUE);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	
		// Headers
		$headers = array();
		$headers []= 'Content-Type: application/x-www-form-urlencoded';
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		
		// Response
		curl_setopt($curl, CURLOPT_HEADER, FALSE);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	
		// Exec
		$curl_response = curl_exec($curl);
			
		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		
		// evaluate for success response
		if ($status != 200) {
		  echo json_encode(array('response'=> array('error'=> 'Error: call to URL ' . EXPERIAN_URL_TOKEN . ' failed with status ' . $status . ', response ' . $curl_response . ', curl_error ' . curl_error($curl) . ', curl_errno ' . curl_errno($curl))));
		  curl_close($curl);
		  return '';
		}
		curl_close($curl);
			
		$json_response = json_decode($curl_response);
		
		if (isset($json_response->response->status) && $json_response->response->status == 'ERROR') {
			echo json_encode($json_response->response);
			return '';
		}
		
		return $json_response->access_token;
	}

	function post_recipients($token, $recipient) {
		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, EXPERIAN_API_RECIPIENTS);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		
		# Post data		
		curl_setopt($curl, CURLOPT_POST, TRUE);
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($recipient));
	
		// Headers
		$headers = array();
		$headers []= 'Content-Type: application/json';
		$headers []= 'Authorization: bearer ' . $token;
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		
		// Response
		curl_setopt($curl, CURLOPT_HEADER, FALSE);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	
		// Exec
		$curl_response = curl_exec($curl);
		
		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		// evaluate for success response
		if ($status != 200) {
		  $errArr = array('response'=> array('error'=> 'Error: call to URL ' . EXPERIAN_URL_TOKEN . ' failed with status ' . $status . ', response ' . $curl_response . ', curl_error ' . curl_error($curl) . ', curl_errno ' . curl_errno($curl)));
		  curl_close($curl);
		  return $errArr;
		}
		curl_close($curl);
		
		return json_decode($curl_response);
	}

}

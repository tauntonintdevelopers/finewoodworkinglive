<?php

class TauntonEmailSubscribe{
	private static $instance = null;
	
	static function init() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new TauntonEmailSubscribe();
		}
	
		return self::$instance;
	}
	
	function __construct() {
		add_action('wp_ajax_taunton_email_subscribe', array( $this, '_ajax_taunton_email_subscribe' ) );
		add_action('wp_ajax_nopriv_taunton_email_subscribe', array( $this, '_ajax_taunton_email_subscribe') );
	}


	function _ajax_taunton_email_subscribe() {
	
		error_log( "TauntonEmailSubscribe::_ajax_taunton_email_subscribe nonce:".$_POST['ajax_nonce']  );

		if( defined( 'TAUNTON_EMAIL_WP_NONCE') )
			check_ajax_referer( TAUNTON_EMAIL_WP_NONCE, 'ajax_nonce' );
		
		error_log( "TauntonEmailSubscribe::_ajax_taunton_email_subscribe data kca_sup passed nonce test");
	
		// Use Experian REST api
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	
			$experian = new experian();
			// The auth token is required
			$token = $experian->get_token();
			
			if ($token === '') {
				die();
			}
	
			try {
	
				$data = array();
				
				$data[] = array("name" => "email", "value" => trim( $_POST['email']));
				$pref = trim( $_POST['pref'] );
				if( !empty( $pref ) ){
					$prefs = explode('|', trim( $pref ) );
					foreach ($prefs as $p ) {
						if (trim ($p) ) {
							$data[] = array("name" => $p, "value" => 100);
						}
					}
				}

				error_log( "TauntonEmailSubscribe::_ajax_taunton_email_subscribe data kca_sup" . $_POST['kca_sup']);
				$kca_sup = $_POST['kca_sup'];
				if( !empty( $kca_sup ) ){
					foreach ($kca_sup as $sup ) {
						if (trim ($sup) ) {
							$data[] = array("name" => "KCA_SUP", "value" => $sup);
						}
					}
				}
				$pro_sup = $_POST['pro_sup'];
				if( !empty( $pro_sup ) ){
					foreach ($pro_sup as $sup ) {
						if (trim ($sup) ) {
							$data[] = array("name" => "PRO_SUP", "value" => $sup);
						}
					}
				}
				
				if( isset(  $_POST['firstname']  ) ){
					$data[] = array("name" => "fname", "value" => trim( $_POST['firstname']));
				}
				if( isset(  $_POST['lastname']  ) ){
					$data[] = array("name" => "lname", "value" => trim( $_POST['lastname']));
				}
				if( isset(  $_POST['source']  ) ){
					$data[] = array("name" => "source", "value" => trim( $_POST['source']));
				}
				error_log( "TauntonEmailSubscribe::_ajax_taunton_email_subscribe data");
				error_log( print_r($data, TRUE) );
				$recipient = array("data" => $data, "apiPostId" => "20");
				$response = $experian->post_recipients($token, $recipient);
				
				echo json_encode($response);
				die();
	
			} catch(Exception $e) {
				error_log( "TauntonEmailSubscribe::_ajax_taunton_email_subscribe nonce: catch error");
				error_log( print_r($e, TRUE) );
				
			}
		}
	
		# DEFAULT
		header('HTTP/1.0 403 Forbidden');
		die();
	
	}
}
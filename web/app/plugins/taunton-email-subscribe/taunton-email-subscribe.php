<?php
/*
Plugin Name: Taunton Email Subscription
Description: Subscribe to email lists with simple email form (just email address)
Version:     0.1
Author:      Taunton Press
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: wporg
Domain Path: /languages
*/

require_once(dirname (__FILE__) . '/config.php');
require_once(dirname (__FILE__) . '/classes/experian.php');
require_once(dirname (__FILE__) . '/classes/taunton_email_subscribe_submit.php');

define( 'TAUNTON_EMAIL_SUBSCRIBE_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

define('TAUNTON_EMAIL_FWW_NEWSLETTER', 'fine_woodworking_eLetter_pref');
define('TAUNTON_EMAIL_FWW_PROMOTION', 'fine_woodworking_offers_pref');
define('TAUNTON_EMAIL_FWWL_NEWSLETTER', 'fine_woodworking_live_pref');
define('TAUNTON_EMAIL_SWW_NEWSLETTER', 'start_woodworking_eLetter_pref');
define('TAUNTON_EMAIL_SWW_PROMOTION', 'start_woodworking_offers_pref');
define('TAUNTON_EMAIL_WP_NONCE', 'ttnemail');


/**
 * Adds Email_Subscribe_Widget widget.
 */
class Taunton_Email_Subscribe_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'Taunton_Email_Subscribe_Widget', // Base ID
			__( 'Email Subscribe', 'text_domain' ), // Name
			array( 'description' => __( 'Add email subscribe form to your page', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
	
		$title = $instance['title'];
	/*
		$pref = $instance['pref'];
		$source = $instance['source'];
		$first_name = $instance['first_name'];
		$last_name = $instance['last_name'];
		$button = $instance['button'];
		$demographic_info = $instance['demographic_info'];
		
		echo $args['before_widget'];
		
		if ( empty( $button ) ) {
			echo 'Please provide a prompt for the submit button';
			return;	
		}
		
		if ( ! empty( $title) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		
		if ( empty( $pref ) ) {
			$pref = site_email_signup_list();	
		}
		
		
		echo '<div class="emailsubscribe__form">';
        echo '<form id="' . $args['widget_id'] . '_form" action="" data-ajax-url="/wp" data-pref="' . $pref . '" data-ajax_nonce="' . taunton_email_wp_nonce() . '"';
		if (! empty($source)) {
			echo  ' data-source="' . $source . '"';
		}
		echo '>';
        echo '<fieldset id="' . $args['widget_id'] . '_formfieldset" class="embed-submit">';
		if ($first_name && $first_name == '1'){
        echo '<div class="formfield">';
		echo '<input class="embed-submit__textfield" id="firstname" name="firstname" type="text" placeholder="First Name" data-msg="">';
        echo '</div>';
		}
		if ($last_name && $last_name == '1'){
        echo '<div class="formfield">';
		echo '<input class="embed-submit__textfield" id="lastname" name="lastname" type="text" placeholder="Last Name" data-msg="">';
        echo '</div>';
		}
		echo '<div class="formfield">';
		echo '<input class="embed-submit__textfield" name="email" type="email" placeholder="Email Address" data-msg="Please enter your email address.">';
        echo '</div>';
		if ($demographic_info){
        echo '<div class="formfield">';
		echo '<p>Tell us more about yourself</p>';
		switch ($demographic_info) {
			case 'KCA_SUP':
				echo '<input type="checkbox" name="'. $demographic_info . '" value="educator" />Educator <br />';
				echo '<input type="checkbox" name="'. $demographic_info . '" value="student" />Student <br />';
				echo '<input type="checkbox" name="'. $demographic_info . '" value="building-design professional" />Building/Design Professional <br />';
				echo '<input type="checkbox" name="'. $demographic_info . '" value="media" />Media <br />';
				echo '<input type="checkbox" name="'. $demographic_info . '" value="supporter" />Supporter <br />';
				break;
		}
        echo '</div>';
		}
        echo '<button class="embed-submit__submit" type="submit">' . $button . '</button>';
        echo '</fieldset>';
		echo '<div id="' . $args['widget_id'] . '_thankyoudiv" style="display:none">';
		echo 'Thank you!';
		echo '</div>';
		echo '<div id="' . $args['widget_id'] . '_errordiv" style="display:none">';
		echo 'Error!  Please contact customer support for assistance!';
		echo '</div>';
        echo '</form>';
        echo '</div>';
        echo '<!-- /form -->';
		echo '<script type="text/javascript">';
		echo '(function($) {';
		echo '$form = $("#' . $args['widget_id'] . '_form");';
		echo 'rules = {email: {';
		echo 'required: true,';
		echo 'email: true}};';
		echo 'messages = {email: {';
		echo 'required: "Please enter your email address",';
		echo 'email: "Emails must contain @ and ."}};';
		echo 'validateSubscribeForm($form, rules, messages, "' . $args['widget_id'] . '");';
		echo '$form.on("submit", function(e) {';
		echo 'response = validateSubscribeForm(this, rules, messages, "' . $args['widget_id'] . '");';
		echo 'e.preventDefault();';
		echo '});}(jQuery));';	
		echo '</script>';
    echo '<!-- /script -->';	
		echo $args['after_widget'];
		*/
		
		echo $args['before_widget'];
		if ( ! empty( $title) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		echo '<div class="button-wrap">';
		echo '<a class="newsletter-cta-button" href="https://www.finewoodworking.com/pages/newsletter-signup?ref=EUsignup" target="_blank">Sign Up</a>';
		echo '</div>';
		echo $args['after_widget'];

	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'New title', 'text_domain' );
		$pref = ! empty( $instance['pref'] ) ? $instance['pref'] : __( '', 'text_domain' );
		$source = ! empty( $instance['source'] ) ? $instance['source'] : __( '', 'text_domain' );
		$button = ! empty( $instance['button'] ) ? $instance['button'] : __( 'Go', 'text_domain' );
		$first_name = ! empty( $instance['first_name'] ) ? $instance['first_name'] : __( '', 'text_domain' );
		$last_name = ! empty( $instance['last_name'] ) ? $instance['last_name'] : __( '', 'text_domain' );
		$demographic_info = ! empty( $instance['demographic_info'] ) ? $instance['demographic_info'] : __( '', 'text_domain' );

		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( esc_attr( 'Title:' ) ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>		
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'pref' ) ); ?>"><?php _e( esc_attr( "Pref: (pipe '|' delimited; leave blank to use site default)" ) ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'pref' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'pref' ) ); ?>" type="text" value="<?php echo esc_attr( $pref ); ?>">
		</p>        
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'source' ) ); ?>"><?php _e( esc_attr( 'Source:' ) ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'source' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'source' ) ); ?>" type="text" value="<?php echo esc_attr( $source ); ?>">
		</p>
        <p>
        <label for="<?php echo esc_attr( $this->get_field_id( 'first_name' ) ); ?>"><?php _e( 'Capture First Name:', 'wp_widget_plugin' ); ?></label>
        <input id="<?php echo esc_attr( $this->get_field_id( 'first_name' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'first_name' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $first_name ); ?> />
        </p>
        <p>
        <label for="<?php echo esc_attr( $this->get_field_id( 'last_name' ) ); ?>"><?php _e( 'Capture Last Name:', 'wp_widget_plugin' ); ?></label>
        <input id="<?php echo esc_attr( $this->get_field_id( 'last_name' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'last_name' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $last_name ); ?> />
        </p>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'button' ) ); ?>"><?php _e( esc_attr( 'Button Text:' ) ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'button' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'button' ) ); ?>" type="text" value="<?php echo esc_attr( $button ); ?>">
		</p>
        <p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'demographic_info' ) ); ?>"><?php _e( esc_attr( 'Demographic Info (field name in experian):' ) ); ?></label> 
		<select id="<?php echo $this->get_field_id('demographic_info'); ?>" name="<?php echo $this->get_field_name('demographic_info'); ?>" class="widefat" style="width:100%;">
            <option <?php selected( $demographic_info, ''); ?> value="">Select One (optional)</option>
            <option <?php selected( $demographic_info, 'KCA_SUP'); ?> value="KCA_SUP">Keep Craft Alive</option>
            <option <?php selected( $demographic_info, 'PRO_SUP'); ?> value="KCA_SUP">ProHome</option>
        </select>
		</p>
<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['pref'] = ( ! empty( $new_instance['pref'] ) ) ? strip_tags( $new_instance['pref'] ) : '';
		$instance['source'] = ( ! empty( $new_instance['source'] ) ) ? strip_tags( $new_instance['source'] ) : '';
		$instance['first_name'] = strip_tags($new_instance['first_name']);
		$instance['last_name'] = strip_tags($new_instance['last_name']);
		$instance['button'] = ( ! empty( $new_instance['button'] ) ) ? strip_tags( $new_instance['button'] ) : '';
		$instance['demographic_info'] = ( ! empty( $new_instance['demographic_info'] ) ) ? strip_tags( $new_instance['demographic_info'] ) : '';

		return $instance;
	}

} // class Email_Subscribe_Widget

// register Email_Subscribe_Widget widget
function register_email_subscribe_widget() {
    register_widget( 'Taunton_Email_Subscribe_Widget' );
}
add_action( 'widgets_init', 'register_email_subscribe_widget' );

// regsiter dependant JS files
function register_email_subscribe_dependancies()
{
    // Register the script
    wp_register_script( 'jquery-validate', plugins_url( '/js/jquery.validate.js', __FILE__ ));
	wp_register_script( 'taunton-email-marketing', plugins_url( '/js/email-subscribe.js', __FILE__ ), array( 'jquery','jquery-validate' ) );
 
    // enqueue the script:
    wp_enqueue_script( 'taunton-email-marketing' );
}
add_action( 'wp_enqueue_scripts', 'register_email_subscribe_dependancies');

add_action( 'init', array( 'TauntonEmailSubscribe', 'init' ) );

//create nonce for experian ajax submit
function taunton_email_wp_nonce(){
	$wp_nonce= '';
	
	$wp_nonce = wp_create_nonce( TAUNTON_EMAIL_WP_NONCE );
	
	return 	$wp_nonce;
}

//Default pref's for fwwl
function site_email_signup_list(){
	$list = array();
	$list_str = '';
	
	$list[] = TAUNTON_EMAIL_FWW_NEWSLETTER;
	$list[] = TAUNTON_EMAIL_FWW_PROMOTION;
	$list[] = TAUNTON_EMAIL_FWWL_NEWSLETTER;

	$list_str = implode( "|", $list );
	
	return $list_str;
}
(function($){
  $(document).ready(function(){
    var $form = $('.contest__form'),
        $submitButton = $form.find('.submit-button'),
        $militaryCheck = $form.find('#military'),
        $militaryFields = $form.find('.military-fields');

    // Makes sure select defaults are not accepted as valid
    $.validator.addMethod("valueNotEquals", function(value, element, arg){
      return arg != value;
    }, "This field is required.");

    $form.validate({
      rules: {
        email: {
          required: true,
          email: true
        },
        firstname: "required",
        lastname: "required",
        address1: "required",
        zip: "required",
        city: "required",
        province: "required",
        country: { valueNotEquals: "default" },
        phone: "required",
        essay: "required",
        rules: "required",
        yearsinservice: {
          required: {
            depends: function(element){
              return $militaryCheck.is(":checked");
            }
          }
        },
        stationed: {
          required: {
            depends: function(element){
              return $militaryCheck.is(":checked");
            }
          }
        },
        branch: {
          required: {
            depends: function(element){
              return $militaryCheck.is(":checked");
            }
          }
        }
      },
      submitHandler: function(form) {
		    //put waiting handling here; thankyou handling is in handle_submit form
        $submitButton.parent().addClass('loading');
        ttn_sweepstakes.handle_submit(form);
      }

    });

    $militaryCheck.on('click', function(){
      $militaryFields.toggleClass('open');
    });

    var provinces = [{"country":"AU","code":"AC","name":"Australian Capital Territory"},{"country":"AU","code":"NS","name":"New South Wales"},{"country":"AU","code":"NT","name":"Northern Territory"},{"country":"AU","code":"QL","name":"Queensland"},{"country":"AU","code":"SA","name":"South Australia"},{"country":"AU","code":"TS","name":"Tasmania"},{"country":"AU","code":"VC","name":"Victoria"},{"country":"AU","code":"WA","name":"Western Australia"},{"country":"CA","code":"AB","name":"Alberta"},{"country":"CA","code":"BC","name":"British Columbia"},{"country":"CA","code":"MB","name":"Manitoba"},{"country":"CA","code":"NB","name":"New Brunswick"},{"country":"CA","code":"NL","name":"Newfoundland and Labrador"},{"country":"CA","code":"NT","name":"Northwest Territories"},{"country":"CA","code":"NS","name":"Nova Scotia"},{"country":"CA","code":"NU","name":"Nunavut"},{"country":"CA","code":"ON","name":"Ontario"},{"country":"CA","code":"PE","name":"Prince Edward Island"},{"country":"CA","code":"QC","name":"Quebec"},{"country":"CA","code":"SK","name":"Saskatchewan"},{"country":"CA","code":"YT","name":"Yukon Territory"},{"country":"NZ","code":"AU","name":"Auckland"},{"country":"NZ","code":"BP","name":"Bay of Plenty"},{"country":"NZ","code":"CB","name":"Canterbury"},{"country":"NZ","code":"GI","name":"Gisborne"},{"country":"NZ","code":"HB","name":"Hawkes Bay"},{"country":"NZ","code":"MW","name":"Manawatu-Wanganui"},{"country":"NZ","code":"MB","name":"Marlborough"},{"country":"NZ","code":"NE","name":"Nelson"},{"country":"NZ","code":"NL","name":"Northland"},{"country":"NZ","code":"OT","name":"Otago"},{"country":"NZ","code":"SL","name":"Southland"},{"country":"NZ","code":"TA","name":"Taranaki"},{"country":"NZ","code":"WA","name":"Waikato"},{"country":"NZ","code":"WE","name":"Wellington"},{"country":"NZ","code":"WC","name":"West Coast"},{"country":"US","code":"AL","name":"Alabama"},{"country":"US","code":"AK","name":"Alaska"},{"country":"US","code":"AS","name":"American Samoa"},{"country":"US","code":"AZ","name":"Arizona"},{"country":"US","code":"AR","name":"Arkansas"},{"country":"US","code":"AA","name":"Armed Forces Americas"},{"country":"US","code":"AE","name":"Armed Forces Europe"},{"country":"US","code":"AP","name":"Armed Forces Pacific"},{"country":"US","code":"CA","name":"California"},{"country":"US","code":"CO","name":"Colorado"},{"country":"US","code":"CT","name":"Connecticut"},{"country":"US","code":"DE","name":"Delaware"},{"country":"US","code":"DC","name":"District of Columbia"},{"country":"US","code":"FM","name":"Federated States Of Micronesia"},{"country":"US","code":"FL","name":"Florida"},{"country":"US","code":"GA","name":"Georgia"},{"country":"US","code":"GU","name":"Guam"},{"country":"US","code":"HI","name":"Hawaii"},{"country":"US","code":"ID","name":"Idaho"},{"country":"US","code":"IL","name":"Illinois"},{"country":"US","code":"IN","name":"Indiana"},{"country":"US","code":"IA","name":"Iowa"},{"country":"US","code":"KS","name":"Kansas"},{"country":"US","code":"KY","name":"Kentucky"},{"country":"US","code":"LA","name":"Louisiana"},{"country":"US","code":"ME","name":"Maine"},{"country":"US","code":"MH","name":"Marshall Islands"},{"country":"US","code":"MD","name":"Maryland"},{"country":"US","code":"MA","name":"Massachusetts"},{"country":"US","code":"MI","name":"Michigan"},{"country":"US","code":"MN","name":"Minnesota"},{"country":"US","code":"MS","name":"Mississippi"},{"country":"US","code":"MO","name":"Missouri"},{"country":"US","code":"MT","name":"Montana"},{"country":"US","code":"NE","name":"Nebraska"},{"country":"US","code":"NV","name":"Nevada"},{"country":"US","code":"NH","name":"New Hampshire"},{"country":"US","code":"NJ","name":"New Jersey"},{"country":"US","code":"NM","name":"New Mexico"},{"country":"US","code":"NY","name":"New York"},{"country":"US","code":"NC","name":"North Carolina"},{"country":"US","code":"ND","name":"North Dakota"},{"country":"US","code":"MP","name":"Northern Mariana Islands"},{"country":"US","code":"OH","name":"Ohio"},{"country":"US","code":"OK","name":"Oklahoma"},{"country":"US","code":"OR","name":"Oregon"},{"country":"US","code":"PW","name":"Palau"},{"country":"US","code":"PA","name":"Pennsylvania"},{"country":"US","code":"PR","name":"Puerto Rico"},{"country":"US","code":"RI","name":"Rhode Island"},{"country":"US","code":"SC","name":"South Carolina"},{"country":"US","code":"SD","name":"South Dakota"},{"country":"US","code":"TN","name":"Tennessee"},{"country":"US","code":"TX","name":"Texas"},{"country":"US","code":"UT","name":"Utah"},{"country":"US","code":"VT","name":"Vermont"},{"country":"US","code":"VI","name":"Virgin Islands"},{"country":"US","code":"VA","name":"Virginia"},{"country":"US","code":"WA","name":"Washington"},{"country":"US","code":"WV","name":"West Virginia"},{"country":"US","code":"WI","name":"Wisconsin"},{"country":"US","code":"WY","name":"Wyoming"}];

    // Fires on selecting a country option
    function country_change(country, province_id, defval) {
        var select = $(province_id),
            optins = $('.options').find('.formfield.opt-in .checkbox');

        select.empty();
        
        if(country == "US") {
          select.append($('<option>').val('').text('Select State'));
          optins.each(function(){
            var $this = $(this);
            $this.attr('checked', 'checked');
          });
        }
        else if(country == "CA") {
          select.append($('<option>').val('').text('Select Province'));
          optins.each(function(){
            var $this = $(this);
            $this.attr('checked', false);
          });
        }

        // Populate the <options>
        for (var i = 0; i < provinces.length; i++) {
          var data = provinces[i];
          if (data['country'] == country) {
            var option = $('<option>').val(data['code']).text(data['name']);
            if (data['code'] == defval) {
              option.prop('selected', true);
            }
            select.append(option);
          }
        }
    }
    
    // Select US by default
    country_change('US', '#province', '');
    // Listener for 'change' event on country <select>
    $('#country').change(function() {
      var country = $(this).val();
      country_change(country, '#province', '');
    });
  });
})(jQuery);
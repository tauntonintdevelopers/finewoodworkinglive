(function($) {
  //ONLY VALID FOR #) DAYS FROM FEB 10 2021; REUSE THIS CODE IF WE NEED TO ALERT AGAIN
  var now = Date.now(),
  promoEnd = Date.parse('March 14, 2021');

  if (now > promoEnd) {
    return;
  }

  var record = getCookie('_updated_pp_2_10_2021');
  if (record == 'Done') {
    return;
  }

  $(function() {
    $('body').append('<link rel="stylesheet" href="/app/themes/enterprise-pro/js/temp/updated-privacy-policy.css" />');

    var html = '<div id="privacy-policy-consent"><div class="inner"><p>We have updated our Privacy Policy; please review it at <a class="open" href="https://www.taunton.com/privacy-policy/" target="_blank">https://www.taunton.com/privacy-policy/</a>. By using our websites and services, and transacting business with us, you are agreeing to our new Privacy Policy.</p><a class="close round-button" href="#">X</a></div></div>';

    $('body').append(html);

    $('#privacy-policy-consent').css('opacity', '1');

    $('#privacy-policy-consent .close').click(function(e){

    	setCookie('_updated_pp_2_10_2021', 'Done', 45);

      $('#privacy-policy-consent').fadeOut();

      e.preventDefault();

    });

  });

}(jQuery));


function setCookie (key, val, expires) {

  Cookies.set(key, val, { expires: expires });

};

function getCookie (key) {

  return Cookies.get(key);

};

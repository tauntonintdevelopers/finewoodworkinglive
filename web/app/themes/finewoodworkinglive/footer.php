<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package finewoodworkinglive
 */

?>

	</div><!-- #content -->
	<div class="sitefooter__block">
        <h4 class="sitefooter__form__heading">Sign up for the Fine Woodworkign eLetter to receive updates about the event.</h4>
        <div class="sitefooter__form">
			<form action="" id="newsletter-signup-footer" data-source="FWWL_footer" data-eid="|214153||214155" data-aid="730585392" data-pid="747639358,756303084">
              <fieldset class="embed-submit">
                <div class="formfield">
                  <input class="embed-submit__textfield" name="email" type="email" placeholder="Email Address" data-msg="Please enter your email address.">
                </div>
                <button class="embed-submit__submit" type="submit">Go</button>
              </fieldset>
            </form>
        </div>
        <!-- /.__form -->
    </div>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			FineWoodWorking Live Footer
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
